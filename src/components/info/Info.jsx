import React from 'react';

import style from './Info.module.css'

function Info() {

    return (
        <>
            <div className={style.container}>
                <h1><a className={style.contacts} href='https://taplink.ru/profile/4509083/pages/' target='_blank'>Contacts</a></h1>
            </div>
        </>
    )
}

export { Info };