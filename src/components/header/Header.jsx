import React from 'react';
import { NavLink } from 'react-router-dom';

import './Header.css';

function Header() {
    return (
        <nav className='nav'>
            <ul className='nav-ul'>
                <li className='nav-left nav-item'>
                    <NavLink exact to='/' activeClassName='nav-active' className='nav-left'>todo-list</NavLink>
                </li>
                <li className='nav-right nav-item'>
                    <NavLink to='/info' activeClassName='nav-active'>info</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export { Header };