import React from 'react'
import styles from './TodoItem.module.css'

function TodoItem({ todo, index, completeTodo, deleteTodo }) {

    return (
        <div className={styles.container}>
            <div className={styles.TodoItem}
                style={{ backgroundColor: todo.isCompleted ? "lightgray" : '' }}
            >

                <div className={styles.left}>

                    <div className={styles.status}
                        onClick={() => completeTodo(index)}
                    >
                        {todo.isCompleted ? <span>&#x2713;</span> : ''}
                    </div>

                    <div className={styles.text}
                        style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
                    >
                        <p className={styles.wrap} >{todo.text}</p>
                    </div>

                </div>

                <div className={styles.right}>
                    <span className={styles.deleteBtn} onClick={() => deleteTodo(index)}>
                        &times;
                    </span>
                </div>

            </div>
        </div>
    )
}

export { TodoItem };
