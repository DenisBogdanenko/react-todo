import React, { useState } from 'react';
import styles from './TodoForm.module.css'

function TodoForm({ addTodo }) {

    const [value, setValue] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        if (!value) return;
        addTodo(value);
        setValue('');
    }

    return (
        <div className={styles.container}>
            <form onSubmit={handleSubmit}>
                <input
                    type='text'
                    placeholder='add your task'
                    className={styles.form}
                    value={value}
                    onChange={e => setValue(e.target.value)}
                />
            </form>
        </div>
    )
}

export { TodoForm }