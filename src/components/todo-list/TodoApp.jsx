import React, { useState } from 'react';
import { TodoItem } from './todo-item/TodoItem';
import { TodoForm } from './todo-form/TodoForm';

import './TodoApp.css'

function TodoApp() {

    const [todos, setTodos] = useState([
        // { text: "Learn about React", isCompleted: false, },
        // { text: "Meet friend for lunch", isCompleted: false, },
        // { text: "Build really cool todo app", isCompleted: false, }
    ]);

    const addTodo = text => {
        const newTodos = [...todos, { text }];
        setTodos(newTodos);
    }

    const completeTodo = index => {
        const newTodos = [...todos];
        newTodos[index].isCompleted = !newTodos[index].isCompleted;
        setTodos(newTodos);
    }

    const deleteTodo = index => {
        const newTodos = [...todos];
        newTodos.splice(index, 1);
        setTodos(newTodos);
    };

    return (
        <>
            <TodoForm addTodo={addTodo} />
            {todos.map((todo, index) => (
                <TodoItem
                    key={index}
                    index={index}
                    todo={todo}
                    completeTodo={completeTodo}
                    deleteTodo={deleteTodo}
                />
            ))}
        </>
    )
}

export { TodoApp };